include_directories(
	${GLEXT_INCLUDE_DIR}
	${OPENGL_INCLUDE_PATH}
	${piglit_SOURCE_DIR}/tests/util
)

link_libraries (
	piglitutil_${piglit_target_api}
	${OPENGL_gl_LIBRARY}
)

piglit_add_executable (amd_gpu_shader_half_float-explicit-offset-bufferstorage half-float-explicit-offset-bufferstorage.c half_float_util.c)

# vim: ft=cmake:
